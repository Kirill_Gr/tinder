//
//  ViewController.swift
//  Tinder
//
//  Created by Nik on 18/04/16.
//  Copyright © 2016 KirillGr. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController, UITextFieldDelegate {

    // MARK: Constants
    
    // MARK: Variables
    
    // MARK: Outlets
    @IBOutlet private var username: UITextField!
    @IBOutlet private var password: UITextField!
    
    // MARK: Properties
    
    // MARK: UIViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        username.delegate = self
        password.delegate = self
        
        //UserBase.sharedInstance.base_ref.unauth()
        UserBase.sharedInstance.LoginUser(self.onUserLogin)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.username.resignFirstResponder()
        self.password.resignFirstResponder()
        return false
    }
    
    // MARK: Actions 
    @IBAction func signUp(sender: AnyObject) {
        if checkLoginTextFields() {
            UserBase.sharedInstance.CreateNewUserWithName(username.text, password: password.text, onLoginAction: self.onUserLogin)
        }
    }
    
    @IBAction func logIn(sender: AnyObject) {
        if checkLoginTextFields() {
            UserBase.sharedInstance.LoginUserWithName(username.text, password: password.text, onLoginAction: self.onUserLogin)
        }
    }
    
    func checkLoginTextFields() -> Bool {
        if username.text == "" || password.text == "" {
            let alert = UIAlertController(title: "Empty field", message: "Please enter username and password", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { action in
                self.dismissViewControllerAnimated(true, completion: nil)
            }))
            self.presentViewController(alert, animated: true, completion: nil)
            return false
        }
        return true
    }
    
    func onUserLogin() {
        self.performSegueWithIdentifier("signUp", sender: self)
        UserBase.sharedInstance.GetUserValues({(userData) -> Void in
            let email = userData.value.objectForKey("email") as? String
            let interestedIn = userData.value.objectForKey("interestedIn") as? String
            print("-------user info-------")
            if email != nil { print("User   -   " + email!) }
            if interestedIn != nil { print("Interested in  " + interestedIn!) }
            print("--------------")
        })
    }
}

