//
//  SignUpViewController.swift
//  Tinder
//
//  Created by Nik on 18/04/16.
//  Copyright © 2016 KirillGr. All rights reserved.
//

import UIKit
import CoreLocation

class SignUpViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, CLLocationManagerDelegate, UITextFieldDelegate {

    // MARK: Constants
    let locationManager = CLLocationManager()
    
    // MARK: Variables
    
    // MARK: Outlets
    @IBOutlet var profilePic: UIImageView!
    @IBOutlet var userGender: UISwitch!
    @IBOutlet var genderSwitch: UISwitch!
    @IBOutlet var nickname: UITextField!
    
    // MARK: Properties
    
    // MARK: UIViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        nickname.delegate = self
        loadUserPrefs()
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
            locationManager.requestLocation()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "toTinder" {
            let next_view: TinderViewController = segue.destinationViewController as! TinderViewController
            var interestedIn = "man"
            if genderSwitch.on {
                interestedIn = "woman"
            }
            next_view.user_interest = interestedIn
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.nickname.resignFirstResponder()
        return false
    }
    
    // MARK: Actions
    @IBAction func signUp(sender: AnyObject) {
        self.performSegueWithIdentifier("toTinder", sender: self)
        var gender = "man"
        var interestedIn = "man"
        if userGender.on {
            gender = "woman"
        }
        if genderSwitch.on {
            interestedIn = "woman"
        }
        var nik = "Rando"
        if nickname.text != "" {
            nik = nickname.text!
        }
        let imageData = UIImageJPEGRepresentation(profilePic.image!, 0.01)
        let base64String = imageData?.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
        UserBase.sharedInstance.SetUserValues(["avatar" : base64String!, "interestedIn" : interestedIn, "nickname" : nik, "gender": gender] as NSDictionary)
        UserBase.sharedInstance.AddUserToSelectionArray(gender)
    }
    
    @IBAction func selectProfilePic(sender: AnyObject) {
        let image_picker_view = UIImagePickerController()
        image_picker_view.delegate = self
        image_picker_view.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        image_picker_view.allowsEditing = false
        self.presentViewController(image_picker_view, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        self.dismissViewControllerAnimated(true, completion: nil)
        profilePic.image = image
    }

    func loadUserPrefs() {
        
        UserBase.sharedInstance.GetUserValues({(userData) -> Void in
            let avatar_data = userData.value.objectForKey("avatar") as? String
            let interested_in = userData.value.objectForKey("interestedIn") as? String
            let gender = userData.value.objectForKey("gender") as? String
            let nick = userData.value.objectForKey("nickname") as? String
            if interested_in != nil && interested_in == "man" {
                self.genderSwitch.on = false
            }
            if gender != nil && gender == "man" {
                self.userGender.on = false
            }
            if nick != nil {
                self.nickname.text = nick
            }
            if avatar_data != nil {
                let decodedData = NSData(base64EncodedString: avatar_data!, options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)
                self.profilePic.image = UIImage(data: decodedData!)
            }
        })
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Error while updating location " + error.localizedDescription)
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let longtitude = String((locations.last?.coordinate.longitude)! as Double)
        let latitude = String((locations.last?.coordinate.latitude)! as Double)
        UserBase.sharedInstance.SetUserValues(["location/longtitude" : longtitude, "location/latitude" : latitude] as NSDictionary)
        print("My coords are " + longtitude + "-" + latitude)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
