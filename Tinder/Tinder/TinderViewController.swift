//
//  TinderViewController.swift
//  Tinder
//
//  Created by Nik on 19/04/16.
//  Copyright © 2016 KirillGr. All rights reserved.
//

import UIKit

class TinderViewController: UIViewController {

    // MARK: Constants
    
    // MARK: Variables
    var user_interest = ""
    
    private var xFromCenter: CGFloat = 0
    private struct TargetUser {
        var uid: String
        var avatar: UIImage
        var nickname: String
    }
    private var loaded_users: [TargetUser] = []
    
    // MARK: Outlets
    
    // MARK: Properties
    
    // MARK: UIViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UserBase.sharedInstance.GetSelectionArray({(data) -> Void in
            let target_users = data.value as! NSDictionary
            for user in target_users {
                print((user.key as! String) + " - " + (user.value as! String))
                if user.value as! String == self.user_interest {
                    self.loadTargetUser(user.key as! String)
                }
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: Actions
    func loadTargetUser(target_uid: String) {
        
        UserBase.sharedInstance.GetTargetUser(target_uid, onGetDataction: {(data) -> Void in
            var avatar: UIImage?
            let avatar_data = data.value.objectForKey("avatar") as? String
            if avatar_data != nil {
                let decodedData = NSData(base64EncodedString: avatar_data!, options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)
                avatar = UIImage(data: decodedData!)!
            }
            let nick = data.value.objectForKey("nickname") as? String
            self.loaded_users.append(TargetUser(uid: data.key, avatar: avatar!, nickname: nick!))
            
            //temp
            if self.loaded_users.count == 1 {
                self.loadNextImage()
            }
        })
    }
    
    func loadNextImage() {
        if loaded_users.count > 0 {
            let next_image: UIImageView = UIImageView(frame: CGRectMake(0, 0, self.view.frame.width, self.view.frame.height))
            next_image.image = loaded_users[0].avatar
            next_image.contentMode = UIViewContentMode.ScaleAspectFit
            next_image.userInteractionEnabled = true
            self.view.addSubview(next_image)
            
            let gesture = UIPanGestureRecognizer(target: self, action: #selector(self.wasDragged(_:)))
            next_image.addGestureRecognizer(gesture)
        }
        else {
            print("THERE ARE NO MORE USERS")
        }
    }
    
    func wasDragged(gesture: UIPanGestureRecognizer) {
        let translation = gesture.translationInView(self.view)
        let subview = gesture.view!
        
        xFromCenter += translation.x
        let scale = min(100 / abs(xFromCenter), 1)
        subview.center = CGPoint(x: subview.center.x + translation.x, y: subview.center.y + translation.y)
        gesture.setTranslation(CGPointZero, inView: self.view)
        let rotation:CGAffineTransform = CGAffineTransformMakeRotation(xFromCenter / 200)
        let stretch:CGAffineTransform = CGAffineTransformScale(rotation, scale, scale)
        subview.transform = stretch
        if subview.center.x < 100 {
            //print("chosen")
        }
        else if subview.center.x > self.view.bounds.width - 100 {
            //print("not chosen")
        }
        if gesture.state == UIGestureRecognizerState.Ended {
            subview.removeFromSuperview()
            loaded_users.removeAtIndex(0)
            loadNextImage()
            xFromCenter = 0
        }
    }
}
