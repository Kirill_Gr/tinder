//
//  UserBase.swift
//  Tinder
//
//  Created by Nik on 20/04/16.
//  Copyright © 2016 KirillGr. All rights reserved.
//

import Foundation
import Firebase

class UserBase {
    static let sharedInstance = UserBase()
    let base_ref = Firebase(url:"https://amber-heat-9491.firebaseio.com/Tinder/")
    
    private var user_uid: String?
    
    init() {
        user_uid = nil
    }
    
    func CreateNewUserWithName(nickname: String!, password: String!, onLoginAction: () -> Void) {
        
        base_ref.createUser(nickname, password: password, withValueCompletionBlock: { error, result in
            if error != nil {
                print("Error while creating new user :   " + error.description)
            }
            else {
                self.user_uid = result["uid"] as! String
                print("Successfully created user account")
                self.LoginUserWithName(nickname, password: password, onLoginAction: onLoginAction)
            }
        })
    }
    
    func LoginUser(onLoginAction: () -> Void) {
        
        base_ref.observeAuthEventWithBlock { (authData) -> Void in
            if authData != nil {
                print("User passed auth  with uid = " + authData.uid)
                self.user_uid = authData.uid
                onLoginAction()
            }
            else {
                print("User didnt pass auth")
            }
        }
    }
    
    func LoginUserWithName(nickname: String!, password: String!, onLoginAction: () -> Void) {
        
        base_ref.authUser(nickname, password: password, withCompletionBlock: { (error, auth) -> Void in
            if error != nil {
                print("Error while auth new user :    " + error.description)
            }
            else {
                print("Login by name was successful")
                self.user_uid = auth.uid
                self.SetUserValues(["email": nickname, "password": password] as NSDictionary)
                onLoginAction()
            }
        })
    }
    
    func GetUserValues(onGetDataAction: (userData: FDataSnapshot) -> Void) {
        let current_user_base = base_ref.childByAppendingPath("users_data/" + user_uid!)
        current_user_base.observeSingleEventOfType(.Value, withBlock: { snapshot in
            onGetDataAction(userData: snapshot)
        }, withCancelBlock: { error in
            print("Error while getting user data :   " + error.description)
        })
    }
    
    func SetUserValues(values: NSDictionary) {
        if values.count > 0 {
            let current_user_base = base_ref.childByAppendingPath("users_data/" + user_uid!)
            current_user_base.updateChildValues(values as [NSObject : AnyObject])
        }
    }
    
    func GetSelectionArray(onGetArrayAction: (data: FDataSnapshot) -> Void) {
        let current_user_base = base_ref.childByAppendingPath("all_users_id/")
        current_user_base.observeSingleEventOfType(.Value, withBlock: { snapshot in
            onGetArrayAction(data: snapshot)
        }, withCancelBlock: { error in
            print("Error while getting selection array :   " + error.description)
        })
    }
    
    func GetTargetUser(target_uid: String ,onGetDataction: (data: FDataSnapshot) -> Void) {
        let current_user_base = base_ref.childByAppendingPath("users_data/" + target_uid)
        current_user_base.observeSingleEventOfType(.Value, withBlock: { snapshot in
            onGetDataction(data: snapshot)
            }, withCancelBlock: { error in
                print("Error while getting target user data :   " + error.description)
        })
    }
    
    func AddUserToSelectionArray(gender: String) {
        if gender != "" {
            let selection_array_base = base_ref.childByAppendingPath("all_users_id/")
            selection_array_base.updateChildValues([self.user_uid! : gender])
        }
    }
}























